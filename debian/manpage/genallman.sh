#!/bin/bash

# Generate several manpages at the same time.
# C 2014 Joao Eriberto Mota Filho <eriberto@debian.org>
# C 2015 Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>
#
# You can use this code in the same terms of the BSD-3-clause license or,
# optionally, in the same terms of the license used in debian/ directory
# in this Debian package. Please, to the last option, refer the package
# name when using.
#
# This script uses txt2man. You need program_name.txt.
#
# The first line of files must be use this structure:
#
# .TH <program_name> "<manpage_level>"  "<date>" "<program_name_upper_case> <program_version>" "<program_description>"
#
# Example:
#
# .TH mac-robber "1"  "May 2013" "MAC-ROBBER 1.02" "collects data about allocated files in mounted filesystems"
MANDIR="debian/manpage"
cd ${MANDIR}

for NAME in $(find . -maxdepth 1 -name "*txt" -printf "%f\n")
do
    LEVEL=$(head -n1 $NAME | cut -d'"' -f2 )
    MAN=$(echo ${NAME%%.txt})
    head -n1 $NAME > $MAN.$LEVEL
    txt2man $NAME | grep -a -v '^.TH ' >> $MAN.$LEVEL
done
